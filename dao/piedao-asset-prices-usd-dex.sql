WITH price AS (

SELECT date_trunc('day', minute) AS day, AVG(price) AS usd_price,
    CASE
       WHEN symbol = 'BCP' THEN 'Balanced Crypto Pie'
       WHEN symbol = 'PLAY' THEN 'Metaverse NFT Index'
       WHEN symbol = 'DEFI++' THEN 'DeFi Index Pie'
       WHEN symbol = 'YPIE' THEN 'Yearn Ecosystem Pie'
       WHEN symbol = 'DOUGH' THEN 'Bake a Cake'
    END AS dex_project
FROM prices.usd
WHERE symbol = 'BCP' OR symbol = 'PLAY' OR symbol = 'DEFI++' OR symbol = 'YPIE' OR symbol = 'DOUGH'
GROUP BY 1, 3
ORDER BY 1 DESC
)

SELECT day, dex_project, usd_price
FROM price
WHERE day >= '2020-10-01'
