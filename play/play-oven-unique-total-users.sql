SELECT date, sum(users) OVER (
                              ORDER BY date ASC ROWS BETWEEN unbounded preceding AND CURRENT ROW) AS total_users
FROM
  (SELECT date, count(USER) AS users
   FROM
     (SELECT min(date) AS date,
             account AS USER
      FROM
        (SELECT block_time AS date,
                "from" AS account
         FROM ethereum."transactions"
         WHERE "to" = '\x0c4Ff8982C66cD29eA7eA96d985f36aE60b85B1C'
           AND block_time >= '2020-12-21 00:00'
           AND success) AS table1
      GROUP BY account) AS table2
   GROUP BY date
   ORDER BY date) AS table3
