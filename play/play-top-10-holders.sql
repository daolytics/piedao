WITH transfers AS (
    SELECT
    evt_tx_hash AS tx_hash,
    tr."from" AS address,
    -tr.value AS amount,
    contract_address
     FROM erc20."ERC20_evt_Transfer" tr
     WHERE contract_address =  '\x33e18a092a93ff21aD04746c7Da12e35D34DC7C4'
UNION ALL
    SELECT
    evt_tx_hash AS tx_hash,
    tr."to" AS address,
    tr.value AS amount,
      contract_address
     FROM erc20."ERC20_evt_Transfer" tr 
     where contract_address = '\x33e18a092a93ff21aD04746c7Da12e35D34DC7C4'
),
transferAmounts AS (
    SELECT address,
    
    sum(amount)/1e18 as balance FROM transfers 
    GROUP BY 1
    ORDER BY 2 DESC
    LIMIT 10
)

SELECT 
(CASE
    when address = '\xaf491d603db3c1138a5ee5db35eb0c7b6c4541a2' then 'SUSHISWAP PLAY-DOUGH'
    when address = '\x0c4ff8982c66cd29ea7ea96d985f36ae60b85b1c' then 'Another Contract'
    else CONCAT('0x', SUBSTRING(address::text, 3))
END) as addressNames,
balance

FROM transferAmounts
