SELECT project,
       date_trunc('day', block_time),
       sum(usd_amount)
FROM dex."trades" t
WHERE block_time > now() - interval '30 days'
  AND ((token_a_address = '\x33e18a092a93ff21ad04746c7da12e35d34dc7c4')
       OR (token_b_address = '\x33e18a092a93ff21ad04746c7da12e35d34dc7c4'))
GROUP BY 1, 2;
