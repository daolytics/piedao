--Forked from https://explore.duneanalytics.com/queries/21433 /0xBoxer

 with search as (SELECT 'BCP'::text as targetindex),
 
 
dex_trades AS (
        SELECT 
            token_a_address as contract_address, 
            usd_amount/token_a_amount as price,
            block_time
        FROM dex.trades, search
        WHERE 1=1
        AND usd_amount  > 0
        AND category = 'DEX'
        AND token_a_amount > 0
        AND (token_a_address  IN (Select asset_address from index.view_indices_assets left join search on 1=1 where index = targetindex)
        or  token_a_address IN (Select token_address from index.view_indices left join search on 1=1 where symbol = targetindex))
        
        UNION ALL 
        
        SELECT 
            token_b_address as contract_address, 
            usd_amount/token_b_amount as price,
            block_time
        FROM dex.trades, search
        WHERE 1=1
        AND usd_amount  > 0
        AND category = 'DEX'
        AND token_a_amount > 0
        AND (token_b_address  IN (Select asset_address from index.view_indices_assets left join search on 1=1 where index = targetindex)
        or  token_b_address IN (Select token_address from index.view_indices left join search on 1=1 where symbol = targetindex))
        
    ),
    
    
    rawdata as (

    SELECT 
        date_trunc('day', block_time) as day,
        d.contract_address,
        e.symbol as asset,
        (PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY price)) AS price,
        count(1) AS sample_size
    FROM dex_trades d
    left join erc20.tokens e on e.contract_address = d.contract_address
    left join search on 1=1
    GROUP BY 1, 2, 3

    UNION ALL
    SELECT
        date_trunc('day', minute) as day,
        contract_address,
        symbol,
        avg(price) AS price,
        10 AS sample_size
        from prices.usd, search
        where symbol= 'WETH' or symbol = 'WBTC'
        group by 1,2,3,5
    )

    ,leaddata as 
    (
    SELECT
    day,
    contract_address,
    asset,
    price,
    sample_size,
    lead(DAY, 1, now() ) OVER (PARTITION BY contract_address ORDER BY day asc) AS next_day
    from rawdata
    where sample_size > 3
    )
    
    ,days AS
    (
    SELECT
    generate_series('2020-01-01'::TIMESTAMP, date_trunc('day', NOW()), '1 day') AS DAY
    )

    ,prices_all_days AS
    (
    SELECT
    d.day as day,
    contract_address,
    asset,
    price,
    sample_size
    from leaddata b
    INNER JOIN days d ON b.day <= d.day
    AND d.day < b.next_day -- Yields an observation for every day after the first transfer until the next day with transfer
    )
    
    ,month as 
    (
    SELECT 
    price as mprice,
    contract_address
    FROM prices_all_days
    where day between now() - interval '31day' and now() - interval '30day'
    )
    
    SELECT
    day,
    p.contract_address,
    asset,
    price, 
    mprice,
    (price/mprice)*100 as performance,
    100 as baseline
    from prices_all_days p
    left join month m on m.contract_address = p.contract_address
    where p.day > now()- interval '31day'
    and p.day < now()- interval '1day'
