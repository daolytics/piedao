-- forked from https://duneanalytics.com/queries/21937/45241

with search as (SELECT 'BCP'::text as targetindex),



transfers AS (
    
    SELECT  day,
            address, 
            token_address, 
            sum(amount) AS amount -- Net inflow or outflow per day
    FROM
    
    (
        SELECT  date_trunc('day', evt_block_time) AS day,
                "to" AS address,
                tr.contract_address AS token_address,
                value AS amount
        FROM erc20."ERC20_evt_Transfer" tr
        WHERE "to" IN (SELECT asset_pool_address from index.view_indices left join search on 1=1 where "symbol" = targetindex) --asset holding pools with filter for pools that don't have productive assets

        UNION ALL
        
        SELECT  date_trunc('day', evt_block_time) AS day,
                "from" AS address,
                tr.contract_address AS token_address,
                -value AS amount
        FROM erc20."ERC20_evt_Transfer" tr
        WHERE "from" IN (SELECT asset_pool_address from index.view_indices left join search on 1=1 where "symbol" = targetindex) --asset holding pools with filter for pools that don't have productive assets
    ) t
   GROUP BY 1, 2, 3
   )

, change_all_days AS (
    SELECT  t.day,
            address,
            erc.symbol,
            t.token_address,
            SUM(amount/10^decimals) AS balance
    FROM transfers t
    INNER JOIN erc20.tokens erc ON t.token_address = erc.contract_address

    GROUP BY 1, 2, 3, 4
    ORDER BY 1, 2, 3, 4
)


, prices as (
 
with dex_trades AS (
        SELECT 
            token_a_address as contract_address, 
            usd_amount/token_a_amount as price,
            block_time
        FROM dex.trades, search
        WHERE 1=1
        AND usd_amount  > 0
        AND category = 'DEX'
        AND token_a_amount > 0
        AND (token_a_address  IN (Select asset_address from index.view_indices_assets left join search on 1=1 where index = targetindex))
        
        UNION ALL 
        
        SELECT 
            token_b_address as contract_address, 
            usd_amount/token_b_amount as price,
            block_time
        FROM dex.trades, search
        WHERE 1=1
        AND usd_amount  > 0
        AND category = 'DEX'
        AND token_a_amount > 0
        AND (token_b_address  IN (Select asset_address from index.view_indices_assets left join search on 1=1 where index = targetindex))
        
    ),
    
    
    rawdata as (

    SELECT 
        date_trunc('day', block_time) as day,
        d.contract_address,
        e.symbol as asset,
        (PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY price)) AS price,
        count(1) AS sample_size
    FROM dex_trades d
    left join erc20.tokens e on e.contract_address = d.contract_address
    left join search on 1=1
    GROUP BY 1, 2, 3

    )

    ,leaddata as 
    (
    SELECT
    day,
    contract_address,
    asset,
    price,
    sample_size,
    lead(DAY, 1, now() ) OVER (PARTITION BY contract_address ORDER BY day asc) AS next_day
    from rawdata
    where sample_size > 3
    )
    
    ,days AS
    (
    SELECT
    generate_series('2020-01-01'::TIMESTAMP, date_trunc('day', NOW()), '1 day') AS DAY
    )


    
    SELECT
    d.day,
    contract_address,
    asset,
    price,
    sample_size
    from leaddata b
    INNER JOIN days d ON b.day <= d.day
    AND d.day < b.next_day -- Yields an observation for every day after the first transfer until the next day with transfer
)
    
 SELECT 
            c.day,
            address,
            symbol,
            c.token_address,
            balance*price
FROM change_all_days c
left join prices p on p.day = c.day and p.contract_address = c.token_address
where c.day < now() -interval '1 day'
