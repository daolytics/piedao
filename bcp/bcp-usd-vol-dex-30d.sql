SELECT project,
       date_trunc('day', block_time),
       sum(usd_amount)
FROM dex."trades" t
WHERE block_time > now() - interval '30 days'
  AND ((token_a_symbol = 'BCP')
       OR (token_b_symbol = 'BCP'))
GROUP BY 1, 2;
