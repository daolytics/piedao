  SELECT
    COALESCE(sum(e.value/1e18), 0) as BCP_Minted
    FROM erc20."ERC20_evt_Transfer" e
    LEFT JOIN ethereum."transactions" tx ON evt_tx_hash = tx.hash
     WHERE "contract_address" = '\xe4f726adc8e89c6a6017f01eada77865db22da14' -- BCP Total
     AND evt_block_time > (DATE_TRUNC('day',CURRENT_TIMESTAMP) - '1 days'::INTERVAL)
        AND e."from" = '\x0000000000000000000000000000000000000000'
