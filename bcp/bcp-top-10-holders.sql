WITH transfers AS (
    SELECT
    evt_tx_hash AS tx_hash,
    tr."from" AS address,
    -tr.value AS amount,
    contract_address
     FROM erc20."ERC20_evt_Transfer" tr
     WHERE contract_address =  '\xe4f726adc8e89c6a6017f01eada77865db22da14'
UNION ALL
    SELECT
    evt_tx_hash AS tx_hash,
    tr."to" AS address,
    tr.value AS amount,
      contract_address
     FROM erc20."ERC20_evt_Transfer" tr 
     WHERE contract_address = '\xe4f726adc8e89c6a6017f01eada77865db22da14'
),
transferAmounts AS (
    SELECT address,
    
    sum(amount)/1e18 as balance FROM transfers 
    GROUP BY 1
    ORDER BY 2 DESC
    LIMIT 10
)

SELECT 
(CASE
    when address = '\xeb1b57d4f7d4557b032b66c422bc94a8e4af859e' then 'SUSHISWAP: BCP'
    when address = '\x674bdf20a0f284d710bc40872100128e2d66bd3f' then 'Loopring: Exchange v2 Deposit'
    when address = '\x9efd60f40e35b3ca7294cc268a35d3e35101be42' then 'Unknown Contract'
    when address = '\xe3d74df89163a8fa1cba540ff6b339d13d322f61' then 'Unknown Contract'
    else CONCAT('0x', SUBSTRING(address::text, 3))
END) as addressNames,
balance

FROM transferAmounts
