SELECT date, sum(users) OVER (
                              ORDER BY date ASC ROWS BETWEEN unbounded preceding AND CURRENT ROW) AS total_users
FROM
  (SELECT date, count(USER) AS users
   FROM
     (SELECT min(date) AS date,
             account AS USER
      FROM
        (SELECT block_time AS date,
                "from" AS account
         FROM ethereum."transactions"
         WHERE "to" = '\xE3d74Df89163A8fA1cBa540FF6B339d13D322F61'
           AND block_time >= '2020-02-12 00:00'
           AND success) AS table1
      GROUP BY account) AS table2
   GROUP BY date
   ORDER BY date) AS table3
