SELECT sum(value / 1e18) AS eth,
       sum(value / 1e18 * price) AS usd
FROM ethereum.transactions
INNER JOIN prices.layer1_usd ON MINUTE = date_trunc('minute', block_time)
AND symbol = 'ETH'
AND "to" = '\xe3d74df89163a8fa1cba540ff6b339d13d322f61'
